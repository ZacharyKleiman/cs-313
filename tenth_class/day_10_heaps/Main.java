public class Main {
    public static void main(String[] args) {
        MinHeap minHeap = new MinHeap(10);

        minHeap.insertMinHeap(3);
        minHeap.insertMinHeap(2);
        minHeap.insertMinHeap(15);
        minHeap.insertMinHeap(5);
        minHeap.insertMinHeap(4);
        minHeap.insertMinHeap(45);

        System.out.println("MinHeap array : " + minHeap.toString());

        System.out.println("Extract Min : " + minHeap.removeMinHeap());

        System.out.println("MinHeap array after removing the minimum : " + minHeap.toString());
    }
}