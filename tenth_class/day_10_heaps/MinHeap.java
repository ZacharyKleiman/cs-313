public class MinHeap {
    int[] heapArray;
    int size;
    int capacity;
    
    public MinHeap() {
        capacity = 20;
        size = 0;
        heapArray = new int[capacity];
    }
    
    public MinHeap(int cap) {
        capacity = cap;
        size = 0;
        heapArray = new int[capacity];
    }
    
    /**
     * Insert in the min-heap array and heapify-up
     * @param num
     */
    public void insertMinHeap(int num) {
        if (size == capacity) {
            System.out.println("Heap is full");
            return;
        }
        heapArray[size] = num; // Insert the element at the end of the heap
        heapifyUp(size); // Heapify-up to maintain the heap property
        size++;
    }
    
    /**
     * Remove the min value from the min-heap
     * and heapify-down
     * @return
     */
    public int removeMinHeap() {
        if (size == 0) {
            System.out.println("Heap is empty");
            return -1;
        }
        int min = heapArray[0]; // The root of the heap is the minimum element
        heapArray[0] = heapArray[size - 1]; // Replace the root with the last element in the heap
        size--;
        heapifyDown(0); // Heapify-down to maintain the heap property
        return min;
    }
    
    /**
     * Once a value is inserted, make sure it's on the right spot.
     * Move it up in necessary.
     * @param index
     */
    public void heapifyUp(int index) {
        int parentIndex = (index - 1) / 2;
        if (index > 0 && heapArray[parentIndex] > heapArray[index]) {
            swap(parentIndex, index);
            heapifyUp(parentIndex);
        }
    }
    
    /**
     * Once the min (root) is removed, replace the root with the last node.
     * Move root down if necessary.
     * @param index
     */
    public void heapifyDown(int index) {
        int leftChildIndex = 2 * index + 1;
        int rightChildIndex = 2 * index + 2;
        int smallest = index;

        if (leftChildIndex < size && heapArray[leftChildIndex] < heapArray[smallest]) {
            smallest = leftChildIndex;
        }

        if (rightChildIndex < size && heapArray[rightChildIndex] < heapArray[smallest]) {
            smallest = rightChildIndex;
        }

        if (smallest != index) {
            swap(index, smallest);
            heapifyDown(smallest);
        }
    }
    
    /**
     * Helper method two swap two nodes in the heap array.
     * @param i
     * @param j
     */
    public void swap(int i, int j) {
        int temp = heapArray[i];
        heapArray[i] = heapArray[j];
        heapArray[j] = temp;
    }
    
    /**
     * Give a string representation of the heap array
     * Example: [1,2,3,4,5]
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            sb.append(heapArray[i]);
            if (i < size - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}