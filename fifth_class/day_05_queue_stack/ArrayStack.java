//queue is last in last out (LILO)
//stack is last in first out (LIFO)

import java.util.Arrays;

public class ArrayStack<E> {
	private E[] stack;
	private int size;
	private final int CAPACITY = 5;
	private int top;
	
	@SuppressWarnings("unchecked")
	public ArrayStack() {
		stack = (E[]) new Object[CAPACITY];
		top = -1;
		size = 0;
	}
	
	/**
	 * Places an element at the top of the stack.
	 * What is the runtime?
	 * @param element
	 */
	public void push(E element) {
		/*if (size==CAPACITY) {
			return;	
		}*/
		    if (size == stack.length) {
        stack = Arrays.copyOf(stack, 2 * size);
    }
		top++;
		stack[top] = element;
		size++;
	}
	
	/**
	 * Removes the element at the top of the stack.
	 * If stack is empty, it returns null.
	 * What is the runtime?
	 * @return E
	 */
	public E pop() {
		if (isEmpty()) {
			return null;
		}
		E temp = stack[top];
		stack[top] = null;
		top--;
		size--;
		return temp;	}
	
	/**
	 * Returns but does not remove the element at the top of the stack.
	 * If stack is empty, it returns null.
	 * What is the runtime?
	 * @return E
	 */
	public E peek() {
		if (isEmpty()) {
			return null;
		}
		return stack[top];
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	public int size() {
		return size;
	}
}
