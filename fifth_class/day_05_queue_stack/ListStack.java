import java.util.LinkedList;
//Last in first out
public class ListStack<E> {
	private java.util.LinkedList<E> stack;
	
	
	@SuppressWarnings("unchecked")
	public ListStack() {
		stack = new LinkedList<>();
	}
	
	/**
	 * Places an element at the top of the stack.
	 * What is the runtime?
	 * @param element
	 */
	public void push(E element) {
		stack.addLast(element);
	}
	
	/**
	 * Removes the element at the top of the stack.
	 * If stack is empty, it returns null.
	 * What is the runtime?
	 * @return E
	 */
	public E pop() {
		if (stack.isEmpty()) {
			return null;
		}
		E temp = stack.getLast();
		stack.removeLast();
		return temp;
	}
	
	/**
	 * Returns but does not remove the element at the top of the stack.
	 * If stack is empty, it returns null.
	 * What is the runtime?
	 * @return E
	 */
	public E peek() {
		if (stack.isEmpty()) {
			return null;
		}
		E temp = stack.getLast();
		return temp;	}
	
	public boolean isEmpty() {
		return stack.isEmpty();
	}
	
	public int size() {
		return stack.size();
	}
}
