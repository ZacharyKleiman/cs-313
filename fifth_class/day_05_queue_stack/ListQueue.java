import java.util.LinkedList;

public class ListQueue<E> {
    LinkedList<E> queue;

    public ListQueue() {
        queue = new LinkedList<>();
    }

    public void enqueue(E element) {
        queue.addLast(element);
    }

    public E dequeue() {
        if (queue.isEmpty()) {
            return null;
        }
        return queue.removeFirst();
    }

    public E front() {
        if (queue.isEmpty()) {
            return null;
        }
        return queue.getFirst();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public int size() {
        return queue.size();
    }
}