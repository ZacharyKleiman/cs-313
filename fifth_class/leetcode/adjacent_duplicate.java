/* You are given a string s consisting of lowercase English letters. A duplicate removal consists of choosing two adjacent and equal 
letters and removing them.

We repeatedly make duplicate removals on s until we no longer can.

Return the final string after all such duplicate removals have been made. It can be proven that the answer is unique.

 

Example 1:

Input: s = "abbaca"
Output: "ca"
Explanation: 
For example, in "abbaca" we could remove "bb" since the letters are adjacent and equal, and this is the only possible move.  
The result of this move is that the string is "aaca", of which only "aa" is possible, so the final string is "ca".
Example 2:

Input: s = "azxxzy"
Output: "ay"
 

*/

class adjacent_duplicate {
    public String removeDuplicates(String s) {
        //iterate over string
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < sb.length() - 1; i++) {
            if (sb.charAt(i) == sb.charAt(i + 1)) {
                sb.delete(i, i + 2);
                i = -1; // After deletion, start the loop from the beginning
            }
        }
        return sb.toString();

    }
    public static void main(String[] args) {
        adjacent_duplicate adjacent_duplicate = new adjacent_duplicate();
        String s = "abbaca";
        System.out.println(adjacent_duplicate.removeDuplicates(s));
    }
}