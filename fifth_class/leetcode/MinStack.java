/*
 * Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

Implement the MinStack class:

MinStack() initializes the stack object.
void push(int val) pushes the element val onto the stack.
void pop() removes the element on the top of the stack.
int top() gets the top element of the stack.
int getMin() retrieves the minimum element in the stack.
You must implement a solution with O(1) time complexity for each function.

 

Example 1:

Input
["MinStack","push","push","push","getMin","pop","top","getMin"]
[[],[-2],[0],[-3],[],[],[],[]]

Output
[null,null,null,null,-3,null,0,-2]

Explanation
MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.getMin(); // return -3
minStack.pop();
minStack.top();    // return 0
minStack.getMin(); // return -2
 */
import java.util.LinkedList;
import java.util.Stack;
class MinStack {
	private java.util.LinkedList<Integer> stack;
    private java.util.LinkedList<Integer> minstack;
    
    public MinStack() {
        stack = new LinkedList<>();
        minstack = new LinkedList<>();

    }
    
    public void push(int val) {
        stack.addLast(val);
        if (minstack.isEmpty() || val <= minstack.peek()) {
            minstack.push(val);
        }
    }
    
    public void pop() {
        if (stack.isEmpty()) {
            return;
        }
        int temp = stack.getLast();
        if (temp==minstack.peek()) {
            minstack.pop();
        }
        stack.removeLast();

    }
    
    public int top() {
        if (stack.isEmpty()) {
            throw new RuntimeException("Stack is empty.");
        }
		int temp = stack.getLast();
		return temp;
    }
    
    public int getMin() {
        if (minstack.isEmpty()) {
            throw new RuntimeException("Stack is empty.");
        }
    
        return minstack.peek();
    }
    public static void main(String[] args) {
        MinStack minstack = new MinStack();
        minstack.push(-2);
        minstack.push(0);
        minstack.push(-3);
        System.out.println(minstack.getMin()); // should print -3
        minstack.pop();
        System.out.println(minstack.top());    // should print 0
        System.out.println(minstack.getMin()); // should print -2
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(val);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */