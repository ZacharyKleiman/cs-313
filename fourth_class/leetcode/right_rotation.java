class ListNode_4 {
    int val;
    ListNode_4 next;
    ListNode_4() {}
    ListNode_4(int val) { this.val = val; }
    ListNode_4(int val, ListNode_4 next) { this.val = val; this.next = next; }
}
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 * Given the head of a linked list, rotate the list to the right by k places.
Input: head = [1,2,3,4,5], k = 2
Output: [4,5,1,2,3]


 */
class right_rotation {
    public static ListNode_4 rotateRight(ListNode_4 head, int k) {
        // If the list is empty or has only one node, the rotated list is the same as the original list.
        if (head == null || head.next == null) return head;

        // Initialize a pointer to the head and calculate the length of the list.
        ListNode_4 old_tail = head;
        int length = 1;
        while (old_tail.next != null) {
            old_tail = old_tail.next;
            length++;
        }

        // Connect the old tail to the head, making the list circular.
        old_tail.next = head;

        // Find the new tail, which is (length - k % length - 1) nodes away from the head.
        ListNode_4 new_tail = head;
        for (int i = 0; i < length - k % length - 1; i++) {
            new_tail = new_tail.next;
        }

        // The new head is the next node of the new tail.
        ListNode_4 new_head = new_tail.next;

        // Disconnect the new tail from the new head.
        new_tail.next = null;

        // Return the new head.
        return new_head;
    }
    public static void main(String[] args) {
        ListNode_4 l1 = new ListNode_4(1);
        l1.next = new ListNode_4(2);
        l1.next.next = new ListNode_4(3);
        l1.next.next.next = new ListNode_4(4);
        l1.next.next.next.next = new ListNode_4(5);
        ListNode_4 l2 = rotateRight(l1, 2);
        while(l2 != null) {
            System.out.println(l2.val);
            l2 = l2.next;
        }
    }
}