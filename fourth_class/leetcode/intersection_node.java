/*
 * Given the heads of two singly linked-lists headA and headB, return the node at which the two lists intersect. If the two linked lists have no intersection at all, return null.

For example, the following two linked lists begin to intersect at node c1:

The test cases are generated such that there are no cycles anywhere in the entire linked structure.

Note that the linked lists must retain their original structure after the function returns.

Custom Judge:

The inputs to the judge are given as follows (your program is not given these inputs):

intersectVal - The value of the node where the intersection occurs. This is 0 if there is no intersected node.
listA - The first linked list.
listB - The second linked list.
skipA - The number of nodes to skip ahead in listA (starting from the head) to get to the intersected node.
skipB - The number of nodes to skip ahead in listB (starting from the head) to get to the intersected node.
The judge will then create the linked structure based on these inputs and pass the two heads, headA and headB to your program. If you correctly return the intersected node, then your solution will be accepted.


 */
public class intersection_node {
    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        while (headA.next!=null && headB.next!=null) {
            if (headA.next == headB.next) {
                return headA.next;
            }
            headA = headA.next;
            headB = headB.next;
        }
        return null;
    }
    public static void main(String[] args) {
        ListNode headA = new ListNode(4);
        ListNode headB = new ListNode(5);
        ListNode headC = new ListNode(6);
        ListNode headD = new ListNode(7);
        ListNode headE = new ListNode(8);
        ListNode headF = new ListNode(9);
        ListNode headG = new ListNode(10);
        headA.next = headB;
        headB.next = headC;
        headC.next = headD;
        headD.next = headE;
        headE.next = headF;
        headF.next = headG;

        ListNode headH = new ListNode(3);
        ListNode headI = new ListNode(4);
        ListNode headJ = new ListNode(5);
        ListNode headK = new ListNode(6);
        
        headH.next = headI;
        headI.next = headJ;
        headJ.next = headK;
        headK.next = headE;    
        System.out.println((getIntersectionNode(headA, headH)).val);
    }
}