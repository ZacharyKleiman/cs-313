/*You are given two non-empty linked lists representing two non-negative integers. 
The digits are stored in reverse order, and each of their nodes contains a single digit. 
Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.
Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

*/



//Definition for singly-linked list.

import java.util.List;

class ListNode_2 {
    int val;
    ListNode_2 next;
    ListNode_2() {}
    ListNode_2(int val) { this.val = val; }
    ListNode_2(int val, ListNode_2 next) { this.val = val; this.next = next; }
}
 
class add_two_nums {
    public ListNode_2 addTwoNumbers(ListNode_2 l1, ListNode_2 l2) {
        ListNode_2 dummyHead = new ListNode_2(0);
        ListNode_2 p = l1, q = l2, curr = dummyHead;
        int carry = 0;
        while (p != null || q != null) {
            int x = (p != null) ? p.val : 0;
            int y = (q != null) ? q.val : 0;
            int sum = carry + x + y;
            carry = sum / 10;
            curr.next = new ListNode_2(sum % 10);
            curr = curr.next;
            if (p != null) p = p.next;
            if (q != null) q = q.next;
        }
        if (carry > 0) {
            curr.next = new ListNode_2(carry);
        }
        return dummyHead.next;
    }
    public static void main(String[] args) {
        ListNode_2 l1 = new ListNode_2(2);
        l1.next = new ListNode_2(4);
        l1.next.next = new ListNode_2(3);
        ListNode_2 l2 = new ListNode_2(5);
        l2.next = new ListNode_2(6);
        l2.next.next = new ListNode_2(4);
        add_two_nums a = new add_two_nums();
        ListNode_2 l3 = a.addTwoNumbers(l1, l2);
        while(l3 != null) {
            System.out.println(l3.val);
            l3 = l3.next;
        }
    }
}

