/*
 * Given a linked list, swap every two adjacent nodes and return its head. 
 * You must solve the problem without modifying the values in the list's nodes (i.e., only nodes themselves may be changed.)


 */

import java.util.List;

class ListNode_3 {
    int val;
    ListNode_3 next;
    ListNode_3() {}
    ListNode_3(int val) { this.val = val; }
    ListNode_3(int val, ListNode_3 next) { this.val = val; this.next = next; }
}

class swap_nodes_pairs {
    public static ListNode_3 swapPairs(ListNode_3 head) {
        // Base case: if the list is empty or has only one node, there's nothing to swap.
        if(head == null || head.next == null) return head;
    
        // Save a reference to the second node.
        ListNode_3 temp = head.next;
    
        // Recursively call swapPairs on the rest of the list starting from the third node.
        // After this call, the rest of the list has been swapped.
        head.next = swapPairs(head.next.next);
    
        // Now we need to swap the first two nodes. We already have a reference to the second node (temp).
        // We make it point to the first node (head).
        temp.next = head;
    
        // Now the second node has become the first node of the swapped pair, so we return it.
        return temp;        
    }
    public static void main(String[] args) {
        ListNode_3 l1 = new ListNode_3(1);
        l1.next = new ListNode_3(2);
        l1.next.next = new ListNode_3(3);
        l1.next.next.next = new ListNode_3(4);
        ListNode_3 l2 = swapPairs(l1);
        while(l2 != null) {
            System.out.println(l2.val);
            l2 = l2.next;
        }
    }
}
