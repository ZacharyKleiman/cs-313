/*
An array nums is monotone increasing if for all i <= j, nums[i] <= nums[j]. An array nums is monotone decreasing if for all i <= j, nums[i] >= nums[j].

*/

class monotonic {
    public static boolean isMonotonic(int[] nums) {
    	//test if increasing
    	for (int i=0;i<nums.length-1;i++) {
    		if (nums[i]>nums[i+1]) {
    			//we find its not increasing. Now we test if decreasing is true
    			for (int j=0;j<nums.length-1;j++) {
    				if (nums[j]<nums[j+1]) {
    					return false; // not true decreasing
    				}
    			}
    			return true; // decreasing
    		}
    	}
	return true; //increasing    	
        
    }
    public static void main(String args[]) {
    	int[] nums= {1,2,2,3};
    	int[] num2 = {6,5,4,4};
    	int[] num3={13,12,97};
    	System.out.println(isMonotonic(nums));
    	System.out.println(isMonotonic(num2));
    	System.out.println(isMonotonic(num3));
    }
}
