/*
 * Given the array nums consisting of 2n elements in the form [x1,x2,...,xn,y1,y2,...,yn].

Return the array in the form [x1,y1,x2,y2,...,xn,yn].

 

Example 1:

Input: nums = [2,5,1,3,4,7], n = 3
Output: [2,3,5,4,1,7] 
Explanation: Since x1=2, x2=5, x3=1, y1=3, y2=4, y3=7 then the answer is [2,3,5,4,1,7].
Example 2:

Input: nums = [1,2,3,4,4,3,2,1], n = 4
Output: [1,4,2,3,3,2,4,1]
Example 3:

Input: nums = [1,1,2,2], n = 2
Output: [1,2,1,2]
 
 */
class shuffle_array {
    public static int[] shuffle(int[] nums, int n) {
        int[] left_nums = new int[n];
        int[] right_nums = new int[n];
        for (int i=0;i<n;i++){
            left_nums[i] = nums[i];
        }
        for (int i=0;i<n;i++){
            right_nums[i] = nums[i+n];
        }
        //merge the two
        int[] result = new int[n*2];
        int j=0;
        while (j<n*2) {
            result[j] = left_nums[j/2];
            result[j+1] = right_nums[j/2];
            j+=2; 


        }
        return result;
    }
    public static void main(String[] args) {
        int[] nums = {2,5,1,3,4,7};
        int n = 3;
        int[] result = shuffle(nums, n);
        for (int i=0;i<result.length;i++){
            System.out.println(result[i]);
        }
    }
}