/*Given a 2D grid of size m x n and an integer k. You need to shift the grid k times.

In one shift operation:

Element at grid[i][j] moves to grid[i][j + 1].
Element at grid[i][n - 1] moves to grid[i + 1][0].
Element at grid[m - 1][n - 1] moves to grid[0][0].
Return the 2D grid after applying shift operation k times.
*/
import java.util.*;
class grid_shift_2d {
    public static List<List<Integer>> shiftGrid(int[][] grid, int k) {
        for (int a=0;a<k;a++) {
            int[][] temp = new int[grid.length][grid[0].length];
            for (int i=0;i<grid.length;i++) {
                for (int j=0;j<grid[0].length;j++) {
                    if (j==grid[0].length-1 && i!=grid.length-1) {
                        temp[i+1][0] = grid[i][j];
                    }
                    else if (i==grid.length-1 && j==grid[0].length-1) {
                        temp[0][0] = grid[i][j];
                    }
                    else {
                        temp[i][j+1] = grid[i][j];
                    }
                }
            }
            grid = temp;

        }
        List<List<Integer>> result = new ArrayList<>();
            for (int[] row : grid) {
            List<Integer> listRow = new ArrayList<>();
            for (int num : row) {
                listRow.add(num);
            }
            result.add(listRow);
    }
return result;
    }
    public static void main(String[] args) {
        int[][] grid = {{1,2,3},{4,5,6},{7,8,9}};
        int k = 50;
        System.out.println(shiftGrid(grid,k));
    }
}