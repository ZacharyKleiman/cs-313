import java.util.*;
class remove_element {
	//this doesn't work right. Fix it
    public static int removeElement(int[] nums, int val) {
        /*
        
        Given an integer array nums and an integer val, remove all occurrences of val in nums in-place. The order of the elements may be changed. Then return the number of elements in nums which are not equal to val.

Consider the number of elements in nums which are not equal to val be k, to get accepted, you need to do the following things:

Change the array nums such that the first k elements of nums contain the elements which are not equal to val. The remaining elements of nums are not important as well as the size of nums.
Return k.

        */
        int k = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
        
    }
    /*
    public static void remove(int index) {
        if (index >= size || index < 0) { // If index is out of bounds
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size); // Throw exception
        }
        System.arraycopy(elements, index + 1, elements, index, size - index - 1); // Shift elements to the left
        size--; // Decrement size
        elements[size] = null; // Let GC do its work
    }*/
    public static void main(String[] args) {
    	int[] nums= {1,2,3,4,2};
    	System.out.println(removeElement(nums,3));
    }

}
