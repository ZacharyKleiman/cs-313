/*
Given an array of integers nums containing n + 1 integers where each integer is in the range [1, n] inclusive.

There is only one repeated number in nums, return this repeated number.

You must solve the problem without modifying the array nums and uses only constant extra space.

*/
class duplicate {
    public static int findDuplicate(int[] nums) {
    	//have hook, [0] by default
    	//count instances
    	//change hook and count all instances after
    	int value=0;

    	int count=0;
    	for (int i=0;i<nums.length;i++) {
    		value = nums[i];
    		for (int j=i;j<nums.length;j++) {
    			if (value == nums[j]) {
    				count++;
    				if ((count) > 1) {
    				return value; // found the doublet
    				}
    			}

    		}
    		count =0;
   
    	}
    	return value;
	        
    }
    public static void main(String[] args) {
    	int[] nums = {3,1,3,4,2};
    	System.out.println(findDuplicate(nums));
    }
}
