import java.util.*;

public class insertion_sort {
    public static void insertionSort(int arr[]) {
        int n = arr.length;
        for (int i = 1; i < n; i++) { // i=1 because first element is already sorted
            int key = arr[i]; // key is the element which is to be inserted at its correct position
            int j = i - 1; // j=i-1 because we have to compare key with previous element
            while (j >= 0 && arr[j] > key) { // if previous element is greater than key
                arr[j + 1] = arr[j]; // then shift the element to right
                j = j - 1; // and decrement j
            }
            arr[j + 1] = key; // insert key at its correct position
        }
    }

    public static void main(String[] args) {
        int[] arr = {213,1321,31,3,413,4134};
        insertionSort(arr);
        System.out.println("Sorted array");
        for (int i : arr) {
            System.out.println(i + " ");
        }
    }
    /*
     * To figure out the time and space complexity of an algorithm yourself, you can follow these steps:

1. **Identify the operations**: Identify the basic operations in the algorithm. In the case of the insertion sort algorithm, the basic operations are the comparisons and the shifts.

2. **Count the operations**: Count how many times the operations are performed in terms of the size of the input. In the insertion sort algorithm, in the worst case, for each element, you might have to shift all the previously sorted elements. This gives you a total of 1 + 2 + 3 + ... + (n-1) operations, which is (n^2 - n) / 2. This simplifies to O(n^2) in Big O notation.

3. **Determine the dominant term**: In the Big O notation, we only keep the term with the highest growth rate when n becomes large. In this case, it's n^2.

4. **Analyze the space usage**: Look at the additional space the algorithm needs (not including the space for the input itself). In the insertion sort algorithm, you only use a constant amount of extra space (for the variables `n`, `i`, `key`, and `j`), so the space complexity is O(1).

Remember, the time complexity measures the time taken by an algorithm to run, as a function of the size of the input to the program. The space complexity measures the maximum space needed by the algorithm to process the input.

For more complex algorithms, you might need to take into account nested loops, recursive calls, etc. The time complexity of these can often be calculated using techniques like the Master Theorem.

     */

}