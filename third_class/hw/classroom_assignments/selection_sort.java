import java.util.Random;
import java.util.stream.IntStream;

public class selection_sort {
    public static void selectionSort(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) { // n-1 because last element will be sorted automatically
            int min = i; // assume first element is minimum
            for (int j = i + 1; j < n; j++) { // j=i+1 because we have already assumed first element is minimum
                if (arr[j] < arr[min]) { // if any element is smaller than assumed minimum element
                    min = j; // then that element will be minimum
                }
            }
            int temp = arr[min]; // swap the minimum element with first element
            arr[min] = arr[i];
            arr[i] = temp;
        }          

    }
    private static int[] generateRandomNumbers(int numNumbers, int upperBound) {
        Random rand = new Random();
        return IntStream.generate(() -> rand.nextInt(upperBound)).limit(numNumbers).toArray();
    }

    public static void main(String[] args) {
        int[] arr = generateRandomNumbers(200, 1000);
        selectionSort(arr);
        System.out.println("Sorted array");
        for (int i : arr) {
            System.out.println(i + " ");
        }
    }
    /*
     * The provided code is an implementation of the Selection Sort algorithm. Here's an analysis of its time and space complexity:

Time Complexity:

Best Case: O(n^2). Even if the array is already sorted, the algorithm still goes through all elements for each element because it doesn't know if the array is sorted.

Average Case: O(n^2). For each element, the algorithm might need to go through about half of the other elements.

Worst Case: O(n^2). For each element, the algorithm will need to go through all the other elements.

The time complexity is O(n^2) in all cases because there are two nested loops in the algorithm, and the inner loop performs n operations for each element in the worst case.

Space Complexity:

The space complexity of Selection Sort is O(1), which means it requires a constant amount of additional space. This is because it is an in-place sorting algorithm and does not require any extra space apart from the input array.
The line where your cursor is (line 20) is part of a helper method that generates an array of random numbers. This method is not part of the selection sort algorithm itself, so it doesn't affect the time and space complexity of the sorting algorithm.


     * 
     */

}