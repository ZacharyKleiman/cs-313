import java.util.ArrayList;
import java.util.Scanner;

public class Tester {
    public static void main(String[] args) {
        // code goes here
        College queensCollege = new College(); // declare a new college
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Select an option:\n1. Admin\n2. Student\n3. Quit");
            int option = scanner.nextInt();
            scanner.nextLine(); // consume the newline left-over

            if (option == 3) {
                break;
            }

            switch (option) {
                case 1:
                    // Admin option
                    System.out.println("Enter a department:");
                    String input = scanner.nextLine();

                    Department department = findDepartment(queensCollege, input);

                    if (department == null) {
                        department = new Department();
                        department.name = input;
                        queensCollege.departments.add(department);
                        System.out.println("Created department: " + input);
                    } else {
                        System.out.println("Enter a class:");
                        String className = scanner.nextLine();
                        Subject subject = findSubject(department, className);
                        if (subject == null) {
                            System.out.println("Enter the number of students:");
                            int students = scanner.nextInt();
                            scanner.nextLine(); // consume the newline left-over
                            department.subjects.add(new Subject(className, students));
                            System.out.println("Added class: " + className + " with " + students + " students");
                        } else {
                            System.out.println("Class already exists in the department.");
                        }
                    }
                    break;

                case 2:
                    // Student option
                    System.out.println("Available classes:");
                    for (Department dep : queensCollege.departments) {
                        for (Subject sub : dep.subjects) {
                            System.out.println(dep.name + " - " + sub.name);
                        }
                    }

                    System.out.println("Enter the department and class name to enroll in:");
                    String depName = scanner.nextLine();
                    String className = scanner.nextLine();

                    Department depToEnroll = findDepartment(queensCollege, depName);
                    if (depToEnroll != null) {
                        Subject classToEnroll = findSubject(depToEnroll, className);
                        if (classToEnroll != null) {
                            classToEnroll.students += 1;
                            System.out.println("Enrolled in class: " + className);
                        } else {
                            System.out.println("Class not found.");
                        }
                    } else {
                        System.out.println("Department not found.");
                    }
                    break;

                default:
                    System.out.println("Invalid option.");
                    break;
            }
        }

        scanner.close();
    }

    private static Subject findSubject(Department department, String name) {
        for (Subject subject : department.subjects) {
            if (subject.name.equalsIgnoreCase(name)) {
                return subject;
            }
        }
        return null;
    }

    private static Department findDepartment(College college, String name) {
        for (Department department : college.departments) {
            if (department.name.equalsIgnoreCase(name)) {
                return department;
            }
        }
        return null;
    }
}
