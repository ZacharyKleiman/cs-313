import java.util.ArrayList;
public class Student extends People {
    // methods and variables specific to Students go here

    public Student() {
        super(); // calling the parent class no-argument constructor
    }

    public Student(ArrayList<Subject> subjects_student, String name) {
        super(subjects_student, name); // calling the parent class constructor with parameters
    }
}