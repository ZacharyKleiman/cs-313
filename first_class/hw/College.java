/*
 * Plan, design and implement a simple class enrollment system (like CUNYFirst)
Create an abstract class College
Create a subclass QueensCollege
Create a subclass CSDepartment
Create a class Subject
Create an abstract class People
Create a subclass Students

The choice of your (static/non-static) variables and methods is up to your creativity. Warning: do not try to make it perfect.

 */

import java.util.ArrayList;

public class College {
    // abstract methods and variables go here
    protected ArrayList<Department> departments;
    protected ArrayList<Student> students;
    protected ArrayList<Subject> subjects;
    public College() {
        departments = new ArrayList<Department>();
        students = new ArrayList<Student>();
        subjects = new ArrayList<Subject>();
    }
    public College (ArrayList<Department> departments, ArrayList<Student> students, ArrayList<Subject> subjects) {
        this.departments = departments;
        this.students = students;
        this.subjects = subjects;
    }

}