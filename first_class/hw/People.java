import java.util.ArrayList;

public abstract class People {
    protected ArrayList<Subject> subjects_student;
    protected String name;

    // No-argument constructor
    public People() {
        this.subjects_student = new ArrayList<>();
        this.name = "";
    }

    // Constructor with arguments
    public People(ArrayList<Subject> subjects_student, String name) {
        this.subjects_student = subjects_student;
        this.name = name;
    }
}