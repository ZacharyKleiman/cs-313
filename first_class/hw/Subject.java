import java.util.ArrayList;

public class Subject {
    public String name;
    public int students;

    public Subject(String name, int students) {
        this.name = name;
        this.students = students;
    }
    public Subject() {
        this.name = "";
        this.students = 0;
    }
}
