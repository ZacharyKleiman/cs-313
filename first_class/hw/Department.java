import java.util.ArrayList;

public class Department {
    protected ArrayList<Subject> subjects;
    protected String name;

    public Department() {
        subjects = new ArrayList<Subject>();
        name = ""; // Initialize name as an empty string
    }

    public Department(ArrayList<Subject> subjects, String name) {
        this.subjects = subjects;
        this.name = name; // Set name to the provided value
    }
    public Department(ArrayList<Subject> subjects) {
        this.subjects = subjects;
    }
}