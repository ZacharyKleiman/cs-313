public class DebitCard extends Card {


    public DebitCard (String cust, String bk, String acnt, int lim, double initialBal) {
        super(cust, bk, acnt, lim, initialBal);

    }

    public DebitCard (String cust, String bk, String acnt, int lim) {
        super (cust,bk,acnt,lim,0.0);
    }
    @Override
    public boolean charge (double price) {
        if (price > limit) {
            return false;
        }
        if (price > balance) {
            return false;
        }
        
        balance +=price;
        return true;
    }

    @Override
    public void makePayment (double amount) {
        balance-=amount;
    }
    public static void printSummary (DebitCard card) {
        System.out.println("Customer = " + card.customer);
        System.out.println("Bank = " + card.bank);
        System.out.println("Account = " + card.account);
        System.out.println("Balance = " + card.balance);
        System.out.println("Limit = " + card.limit);
    }
    public static void main(String[] args) {
        DebitCard[] wallet = new DebitCard[3];
        wallet[0] = new DebitCard("John Bowman", "California Savings", "5391 0375 9387 5309", 5000);
        wallet[1] = new DebitCard("John Bowman", "California Federal", "3485 0399 3395 1954", 3500);
        wallet[2] = new DebitCard("John Bowman", "California Finance", "5391 0375 9387 5309", 2500, 350);   
        for (int val=1; val<=16;val++) {
            wallet[0].charge(3*val);
            wallet[1].charge(2*val);
            wallet[2].charge(val);
            System.out.println(val);
        }
        for (DebitCard card: wallet) {
            DebitCard.printSummary(card);
            while (card.getBalance() > 200.0) {
                card.makePayment(200);
                System.out.println("New balance = " + card.getBalance());

            }
        }
    }
}