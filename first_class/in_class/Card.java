public abstract class Card {
    protected String customer;
    protected String bank;
    protected String account;
    protected int limit;
    protected double balance;
    public Card (String cust, String bk, String acnt, int lim, double initialBal) {
        customer = cust;
        bank = bk;
        account = acnt;
        limit=lim;
        balance=initialBal;

    }
    public String getCustomer() {return customer;}
    public String getBank() {return bank;}
    public String getAccount() {return account;}
    public int getLimit() {return limit;}
    public double getBalance() {return balance;}
    public abstract boolean charge (double price);
    public abstract void makePayment (double amount);


}
