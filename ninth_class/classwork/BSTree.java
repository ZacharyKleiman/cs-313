import java.util.LinkedList;

public class BSTree<E extends Comparable<E>> {
    TreeNode<E> root;
    int size;

    public BSTree() {
        root = null;
        size = 0;
    }

    public E element(TreeNode<E> node) {
        if(node == null) return null;
        return node.data;
    }

    public TreeNode<E> getParent(TreeNode<E> node) {
        if(node == null || node == root) return null;
        return node.parent;
    }

    public LinkedList<TreeNode<E>> getChildren(TreeNode<E> node) {
        LinkedList<TreeNode<E>> children = new LinkedList<>();
        if (node.leftchild != null) {
            children.add(node.leftchild);
        }
        if (node.rightchild != null) {
            children.add(node.rightchild);
        }
        return children;
    }

    public boolean isRoot(TreeNode<E> node) {
        return node == root;
    }

    public boolean isExternal(TreeNode<E> node) {
        return node.leftchild == null && node.rightchild == null;
    }

    public boolean isInternal(TreeNode<E> node) {
        return node.leftchild != null || node.rightchild != null;
    }

    public int nodeDepth(TreeNode<E> node) {
        int depth = 0;
        while (node != root) {
            node = node.parent;
            depth++;
        }
        return depth;
    }

    public int treeHeight() {
        return treeHeight(root);
    }

    private int treeHeight(TreeNode<E> node) {
        if (node == null) {
            return -1;
        }
        return 1 + Math.max(treeHeight(node.leftchild), treeHeight(node.rightchild));
    }

    public int size() {
        return size;
    }

    public void insert(E data) {
        root = insert(root, data, null);
    }

    private TreeNode<E> insert(TreeNode<E> node, E data, TreeNode<E> parent) {
        if (node == null) {
            size++;
            TreeNode<E> newNode = new TreeNode<>(data);
            newNode.parent = parent;
            return newNode;
        }
        if (data.compareTo(node.data) < 0) {
            node.leftchild = insert(node.leftchild, data, node);
        } else if (data.compareTo(node.data) > 0) {
            node.rightchild = insert(node.rightchild, data, node);
        }
        return node;
    }
    public static void main(String[] args) {
        BSTree<Integer> tree = new BSTree<>();
        tree.insert(5);
        tree.insert(3);
        tree.insert(7);
        tree.insert(2);
        tree.insert(4);
        tree.insert(6);
        tree.insert(8);
    
        // Testing treeHeight
        System.out.println("Tree height: " + tree.treeHeight());
    
        // Testing size
        System.out.println("Tree size: " + tree.size());
    
        // Testing element
        System.out.println("Root element: " + tree.element(tree.root));
    
        // Testing getParent
        System.out.println("Parent of root: " + tree.getParent(tree.root));
    
// Testing getChildren
LinkedList<TreeNode<Integer>> children = tree.getChildren(tree.root);
System.out.print("Children of root: ");
for (TreeNode<Integer> child : children) {
    System.out.print(child + " ");
}
System.out.println(); 
        
        // Testing isRoot
        System.out.println("Is root a root? " + tree.isRoot(tree.root));
    
        // Testing isExternal
        System.out.println("Is root external? " + tree.isExternal(tree.root));
    
        // Testing isInternal
        System.out.println("Is root internal? " + tree.isInternal(tree.root));
    
        // Testing nodeDepth
        System.out.println("Depth of root: " + tree.nodeDepth(tree.root));
    }
}