
public class TreeNode<E>{
	E data;
	TreeNode<E> leftchild;
	TreeNode<E> rightchild;
    int height;
	
	public TreeNode(E e) {
		data = e;
		leftchild = null;
		rightchild = null;
		height = 1;  // New nodes are leaf nodes, height = 1

	}
	@Override
    public String toString() {
        return String.valueOf(data);
    }
}
