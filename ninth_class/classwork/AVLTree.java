import java.util.LinkedList;

public class AVLTree<E extends Comparable<E>> {
    TreeNode<E> root;
    int size;

    public AVLTree() {
        root = null;
        size = 0;
    }

    // ... your existing code ...

    public void insert(E data) {
        root = insert(root, data);
    }

    private TreeNode<E> insert(TreeNode<E> node, E data) {
        // Perform standard BST insert
        if (node == null) {
            size++;
            return new TreeNode<>(data);
        }
        if (data.compareTo(node.data) < 0) {
            node.leftchild = insert(node.leftchild, data);
        } else if (data.compareTo(node.data) > 0) {
            node.rightchild = insert(node.rightchild, data);
        } else {
            // Duplicate data not allowed
            return node;
        }

        // Update height of this ancestor node
        node.height = 1 + Math.max(height(node.leftchild), height(node.rightchild));

        // Get the balance factor
        int balance = getBalance(node);

        // If this node becomes unbalanced, then there are 4 cases

        // Left Left Case
        if (balance > 1 && data.compareTo(node.leftchild.data) < 0) {
            return rightRotate(node);
        }

        // Right Right Case
        if (balance < -1 && data.compareTo(node.rightchild.data) > 0) {
            return leftRotate(node);
        }

        // Left Right Case
        if (balance > 1 && data.compareTo(node.leftchild.data) > 0) {
            node.leftchild = leftRotate(node.leftchild);
            return rightRotate(node);
        }

        // Right Left Case
        if (balance < -1 && data.compareTo(node.rightchild.data) < 0) {
            node.rightchild = rightRotate(node.rightchild);
            return leftRotate(node);
        }

        return node;
    }

    private TreeNode<E> rightRotate(TreeNode<E> y) {
        TreeNode<E> x = y.leftchild;
        TreeNode<E> T2 = x.rightchild;

        // Perform rotation
        x.rightchild = y;
        y.leftchild = T2;

        // Update heights
        y.height = Math.max(height(y.leftchild), height(y.rightchild)) + 1;
        x.height = Math.max(height(x.leftchild), height(x.rightchild)) + 1;

        // Return new root
        return x;
    }

    private TreeNode<E> leftRotate(TreeNode<E> x) {
        TreeNode<E> y = x.rightchild;
        TreeNode<E> T2 = y.leftchild;

        // Perform rotation
        y.leftchild = x;
        x.rightchild = T2;

        // Update heights
        x.height = Math.max(height(x.leftchild), height(x.rightchild)) + 1;
        y.height = Math.max(height(y.leftchild), height(y.rightchild)) + 1;

        // Return new root
        return y;
    }

    private int getBalance(TreeNode<E> node) {
        if (node == null) {
            return 0;
        }
        return height(node.leftchild) - height(node.rightchild);
    }

    private int height(TreeNode<E> node) {
        if (node == null) {
            return 0;
        }
        return node.height;
 
    }
    public static void main(String[] args) {
        AVLTree<Integer> avlTree = new AVLTree<>();
    
        avlTree.insert(10);
        avlTree.insert(20);
        avlTree.insert(30);
        avlTree.insert(40);
        avlTree.insert(50);
        avlTree.insert(25);
    
        // Print the root of the AVL tree
        System.out.println("Root of the AVL tree is: " + avlTree.root.data);
    
        // Print the height of the AVL tree
        System.out.println("Height of the AVL tree is: " + avlTree.height(avlTree.root));
    
        // Print the size of the AVL tree
        System.out.println("Size of the AVL tree is: " + avlTree.size);
    }
}