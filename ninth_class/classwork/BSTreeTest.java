
import static org.junit.Assert.*;
import org.junit.Test;

import day_04_lists.LinkedList;
import day_04_lists.Node;

//import day_07_trees.Tree.TreeNode;

public class BSTreeTest {

    @Test
    public void testGetParent() {
        BSTree<String> tree = new BSTree<>();
        TreeNode<String> root = new TreeNode<>("Root");
        TreeNode<String> child = new TreeNode<>("Child");

        assertNull(tree.getParent(root));
        assertNull(tree.getParent(child));
    }

    @Test
    public void testGetChildren() {
        BSTree<Character> tree = new BSTree<>();
        TreeNode<Character> root = new TreeNode<>('A');
        TreeNode<Character> child1 = new TreeNode<>('B');
        TreeNode<Character> child2 = new TreeNode<>('C');

        root.leftchild = child1;
        root.rightchild = child2;

        LinkedList<TreeNode<Character>> children = tree.getChildren(root);
        assertNotNull(children);
        assertEquals(2, children.getLength());
        assertTrue(children.isPresent(new Node(child1)));
        assertTrue(children.isPresent(new Node(child2)));
    }

    @Test
    public void testIsRoot() {
        BSTree<Double> tree = new BSTree<>();
        TreeNode<Double> root = new TreeNode<>(3.14);

        assertTrue(tree.isRoot(root));
    }

    @Test
    public void testIsExternal() {
        BSTree<String> tree = new BSTree<>();
        TreeNode<String> externalNode = new TreeNode<>("External");

        assertTrue(tree.isExternal(externalNode));
    }

    @Test
    public void testIsInternal() {
        BSTree<Integer> tree = new BSTree<>();
        TreeNode<Integer> internalNode = new TreeNode<>(42);

        assertTrue(tree.isInternal(internalNode));
    }

    @Test
    public void testNodeDepth() {
        BSTree<String> tree = new BSTree<>();
        TreeNode<String> root = new TreeNode<>("Root");
        TreeNode<String> child1 = new TreeNode<>("Child1");
        TreeNode<String> child2 = new TreeNode<>("Child2");

        root.leftchild = child1;
        child1.leftchild = child2;

        assertEquals(0, tree.nodeDepth(root));
        assertEquals(1, tree.nodeDepth(child1));
        assertEquals(2, tree.nodeDepth(child2));
    }

    @Test
    public void testTreeHeight() {
        BSTree<Integer> tree = new BSTree<>();
        TreeNode<Integer> root = new TreeNode<>(10);
        TreeNode<Integer> child1 = new TreeNode<>(20);
        TreeNode<Integer> child2 = new TreeNode<>(30);
        TreeNode<Integer> grandchild1 = new TreeNode<>(40);
        TreeNode<Integer> grandchild2 = new TreeNode<>(50);

        root.leftchild = child1;
        root.rightchild = child2;
        child1.leftchild = grandchild1;
        child2.rightchild = grandchild2;

        assertEquals(3, tree.treeHeight());
    }

    @Test
    public void testSize() {
        BSTree<String> tree = new BSTree<>();
        TreeNode<String> root = new TreeNode<>("Root");
        TreeNode<String> child1 = new TreeNode<>("Child1");
        TreeNode<String> child2 = new TreeNode<>("Child2");

        root.leftchild = child1;
        root.rightchild = child2;

        assertEquals(3, tree.size());
    }
}

