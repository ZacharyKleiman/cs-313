/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {}

    TreeNode(int val) { 
        this.val = val; 
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
public class invert_binary_tree {
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }

        // Invert the subtrees
        TreeNode left = invertTree(root.left);
        TreeNode right = invertTree(root.right);

        // Swap the left and right subtrees
        root.left = right;
        root.right = left;

        return root;
    }

    public static void main(String[] args) {
        // Create a binary tree
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);

        // Print original tree
        System.out.println("Original tree in pre-order: ");
        preOrder(root);
        System.out.println();

        // Invert the tree
        invert_binary_tree invertor = new invert_binary_tree();
        TreeNode invertedRoot = invertor.invertTree(root);

        // Print inverted tree
        System.out.println("Inverted tree in pre-order: ");
        preOrder(invertedRoot);
        System.out.println();
    }

    // Helper method to print the tree in pre-order
    public static void preOrder(TreeNode root) {
        if (root != null) {
            System.out.print(root.val + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }
}