/**
Given the root of a binary tree, return the same tree where every subtree (of the given tree) not containing a 1 has been removed.

A subtree of a node node is node plus every node that is a descendant of node.


 * Definition for a binary tree node.
 */
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class binary_pruning {
    public TreeNode pruneTree(TreeNode root) {
        if (root == null) return null;
        root.left = pruneTree(root.left);
        root.right = pruneTree(root.right);
        if (root.val == 0 && root.left == null && root.right == null) return null;
        return root;
    }

    public void printTree(TreeNode root) {
        if (root == null) return;
        System.out.print(root.val + " ");
        printTree(root.left);
        printTree(root.right);
    }

    public static void main(String[] args) {
        // Create a binary tree
        TreeNode leftSubTree = new TreeNode(0, new TreeNode(0), new TreeNode(0));
        TreeNode rightSubTree = new TreeNode(1, new TreeNode(0), new TreeNode(1));
        TreeNode root = new TreeNode(1, leftSubTree, rightSubTree);

        // Create an instance of the class
        binary_pruning bp = new binary_pruning();
        // Print the original tree
        System.out.println("Original tree:");
        bp.printTree(root);
        System.out.println();

        // Prune the tree
        TreeNode prunedTree = bp.pruneTree(root);
        // Print the pruned tree
        System.out.println("Pruned tree:");
        bp.printTree(prunedTree);
        System.out.println();
    }
}