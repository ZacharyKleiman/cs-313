// Definition for a binary tree node.
class TreeNode {
    int val; // Node value
    TreeNode left; // Left child
    TreeNode right; // Right child
    TreeNode() {} // Default constructor
    TreeNode(int val) { this.val = val; } // Constructor that sets node value
    TreeNode(int val, TreeNode left, TreeNode right) { // Constructor that sets node value and children
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

public class validate_binary_tree {
    // Method to check if a binary tree is a valid binary search tree (BST)
    public boolean isValidBST(TreeNode root) {
        // A null tree is valid BST
        if (root == null) return true;
        // Check if left and right subtrees are valid BSTs with appropriate bounds
        return isValidBST (root.left, null, root.val) && isValidBST (root.right, root.val, null);
    }

    // Helper method to check if a binary tree is a valid BST within given bounds
    private boolean isValidBST(TreeNode node, Integer lower, Integer upper) {
        // A null tree is valid BST
        if (node == null) return true;
        int val = node.val;
        // Check if node value is within the bounds
        if (lower != null && val <= lower) return false;
        if (upper != null && val >= upper) return false;

        // Check if right and left subtrees are valid BSTs with updated bounds
        if (!isValidBST(node.right, val, upper)) return false;
        if (!isValidBST(node.left, lower, val)) return false;
        return true;
    }

    // Main method to test the isValidBST method
    public static void main(String[] args) {
        // Create a binary tree
        TreeNode leftSubTree = new TreeNode(2, new TreeNode(1), new TreeNode(3));
        TreeNode root = new TreeNode(4, leftSubTree, new TreeNode(5));

        // Create an instance of the class
        validate_binary_tree vbt = new validate_binary_tree();
        // Print the result of the isValidBST method
        System.out.println(vbt.isValidBST(root));  // Should print: true
    }
}