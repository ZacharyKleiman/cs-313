/**
 * Definition for a binary tree node.
 */
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class balanced_binary_tree {
    // Check if the tree is balanced
    public boolean isBalanced(TreeNode root) {
        // A null tree is balanced
        if (root == null) {
            return true;
        }

        // Get the height of left and right subtrees
        int leftHeight = height(root.left);
        int rightHeight = height(root.right);

        // Check if current node is balanced (difference between heights <= 1)
        // and recursively check if left and right subtrees are balanced
        return Math.abs(leftHeight - rightHeight) <= 1 && isBalanced(root.left) && isBalanced(root.right);
    }

    // Helper method to calculate the height of the tree
    private int height(TreeNode node) {
        // A null node has height -1
        if (node == null) {
            return -1;
        }

        // Height of the tree is max height of left/right subtrees + 1
        return Math.max(height(node.left), height(node.right)) + 1;
    }


    public static void main(String[] args) {
        TreeNode leftSubTree = new TreeNode(2, new TreeNode(3), null);
        TreeNode rightSubTree = new TreeNode(2, null, new TreeNode(3));
        TreeNode root = new TreeNode(1, leftSubTree, rightSubTree);

        balanced_binary_tree bbt = new balanced_binary_tree();
        System.out.println(bbt.isBalanced(root));  // Should print: true
    }
}