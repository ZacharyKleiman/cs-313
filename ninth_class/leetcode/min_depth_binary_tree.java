import java.util.ArrayList;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class min_depth_binary_tree {
    ArrayList<Integer> list = new ArrayList<Integer>();

    public int minDepth(TreeNode root) {
        return minDepthHelper(root, 0);
    }

    private int minDepthHelper(TreeNode root, int depth) {
        if (root == null) {
            return depth;
        }
        int leftDepth = minDepthHelper(root.left, depth + 1);
        int rightDepth = minDepthHelper(root.right, depth + 1);
        // If one of the subtrees is not present, return the depth of the other.
        if (root.left == null || root.right == null) {
            return Math.max(leftDepth, rightDepth);
        }
        return Math.min(leftDepth, rightDepth);
    }

    public static void main(String[] args) {
        // Create a binary tree
        TreeNode leftSubTree = new TreeNode(2, new TreeNode(1), new TreeNode(3));
        TreeNode root = new TreeNode(4, leftSubTree, new TreeNode(5));

        // Create an instance of the class
        min_depth_binary_tree mdbt = new min_depth_binary_tree();
        // Print the result of the minDepth method
        System.out.println(mdbt.minDepth(root));  // Should print: 2
    }


}