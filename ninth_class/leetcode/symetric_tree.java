/**
 * Definition for a binary tree node.
*/
 class TreeNode {
     int val;
     TreeNode left;
     TreeNode right;
     TreeNode() {}
     TreeNode(int val) { this.val = val; }
     TreeNode(int val, TreeNode left, TreeNode right) {
         this.val = val;
        this.left = left;
        this.right = right;
     }
 }
 
class symetric_tree {
    public boolean isSymmetric(TreeNode root) {
        return root == null || isMirror(root.left, root.right);
    }

    private boolean isMirror(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) return true;
        if (t1 == null || t2 == null) return false;
        return (t1.val == t2.val)
            && isMirror(t1.right, t2.left)
            && isMirror(t1.left, t2.right);
    }
    public static void main(String[] args) {
        TreeNode leftSubTree = new TreeNode(2, new TreeNode(3), new TreeNode(4));
        TreeNode rightSubTree = new TreeNode(2, new TreeNode(4), new TreeNode(3));
        TreeNode root = new TreeNode(1, leftSubTree, rightSubTree);

        symetric_tree st = new symetric_tree();
        System.out.println(st.isSymmetric(root));  // Should print: true
    }

}