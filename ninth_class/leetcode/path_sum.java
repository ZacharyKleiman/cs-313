/*
 * Given the root of a binary tree and an integer targetSum, return true if the tree has a root-to-leaf path such that adding up all 
 * the values along the path equals targetSum.

A leaf is a node with no children.


 */
/**
 Definition for a binary tree node.
*/
class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

class path_sum {
    public boolean hasPathSum(TreeNode root, int targetSum) {
        // If the node is null, return false
        if (root == null) {
            return false;
        }

        // If the node is a leaf and its value is equal to the target sum, return true
        if (root.left == null && root.right == null && root.val == targetSum) {
            return true;
        }

        // Recursively check the left and right subtrees, reducing the target sum by the current node's value
        return hasPathSum(root.left, targetSum - root.val) || hasPathSum(root.right, targetSum - root.val);
    }

   public static void main(String[] args) {
    TreeNode leftSubTree = new TreeNode(4, new TreeNode(11, new TreeNode(7), new TreeNode(2)), null);
    TreeNode rightSubTree = new TreeNode(8, new TreeNode(13), new TreeNode(4, null, new TreeNode(1)));
    TreeNode root = new TreeNode(5, leftSubTree, rightSubTree);

    path_sum ps = new path_sum();
    System.out.println(ps.hasPathSum(root, 22));  // Should print: true
}

}