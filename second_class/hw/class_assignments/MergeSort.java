import java.util.Arrays;
public class MergeSort {
    public static void mergeSort(int[] arr) {
        if (arr.length < 2) {
            return;
        }
        int mid = arr.length / 2;
        int[] left = new int[mid];
        int[] right = new int[arr.length - mid];

        // System.arraycopy is a native method that's more efficient than a Java loop for copying arrays.
        // It takes five parameters:
        // 1. The source array.
        // 2. The starting position in the source array.
        // 3. The destination array.
        // 4. The starting position in the destination array.
        // 5. The number of elements to copy.
        System.arraycopy(arr, 0, left, 0, mid);
        System.arraycopy(arr, mid, right, 0, arr.length - mid);

        mergeSort(left);
        mergeSort(right);
        merge(arr, left, right);
    }

    public static void merge(int[] arr, int[] left, int[] right) {
        int i = 0, j = 0, k = 0;
        while (i < left.length && j < right.length) {
            if (left[i] <= right[j]) {
                arr[k++] = left[i++];
            } else {
                arr[k++] = right[j++];
            }
        }
        while (i < left.length) {
            arr[k++] = left[i++];
        }
        while (j < right.length) {
            arr[k++] = right[j++];
        }
    }

    public static void main(String[] args) {
        int[] arr = {6, 5, 3, 1, 8, 7, 2, 4};
        mergeSort(arr);
        for (int i : arr) {
            System.out.println(i + " ");
        }
    }
    /*
     * The Merge Sort algorithm performs approximately n*log(n) operations to sort an array of size n. Here's how it works for an array of size 4:

1. **Divide**: The algorithm divides the array into two halves recursively, until it cannot be divided further. This division step takes log(n) time because we're repeatedly dividing the array in half. For an array of size 4, this results in 2 levels of division (4 -> 2 -> 1), so log(4) = 2 divisions.

2. **Conquer/Merge**: After the division, the algorithm starts merging the subarrays. Each merge operation takes linear time, i.e., O(n), because it involves comparing elements and copying them into a temporary array. Since we have to merge n elements for each level of the recursion, and there are log(n) levels, the total time for this step is n log(n).

So, for an array of size 4, the total number of operations is approximately 4 * log(4) = 4 * 2 = 8 operations. This includes both the comparison and the copying operations during the merge step.

Please note that this is a simplification. The actual number of operations can vary depending on the specifics of the implementation and the data in the array. But in terms of time complexity, we say that Merge Sort is O(n log n).
     */
}

