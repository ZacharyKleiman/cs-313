//add, remove, isEmpty, size, insert, toArray
public class ExpendableArray<E> {
    private Object[] elements; // Array to store elements
    private int size = 0; // Number of elements in the list

    public ExpendableArray() {
        elements = new Object[10]; // Initialize array with initial capacity of 10
    }

    public void add(E e) {
        if (size == elements.length) { // If array is full
            resize(); // Resize the array
        }
        elements[size++] = e; // Add element at the end of the array and increment size
    }
    public boolean isEmpty() {
        return size ==0;

    }
    public int size(E e) {
        return size;
    }
    public void insert(int index, E e) {
        if (index > size || index < 0) { // If index is out of bounds
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size); // Throw exception
        }
        if (size == elements.length) { // If array is full
            resize(); // Resize the array
        }
        System.arraycopy(elements, index, elements, index + 1, size - index); // Shift elements to the right
        elements[index] = e; // Insert element at index
        size++; // Increment size
    }
    public void remove(int index) {
        if (index >= size || index < 0) { // If index is out of bounds
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size); // Throw exception
        }
        System.arraycopy(elements, index + 1, elements, index, size - index - 1); // Shift elements to the left
        size--; // Decrement size
        elements[size] = null; // Let GC do its work
    }
    
    @SuppressWarnings("unchecked")
    public E[] toArray(E[] a) {
        if (a.length < size) {
            // If array is too small, create a new one with the same runtime type and the size of the list
            a = (E[]) java.lang.reflect.Array.newInstance(a.getClass().getComponentType(), size);
        }
        System.arraycopy(elements, 0, a, 0, size);
        if (a.length > size) {
            a[size] = null;  // If array is larger, set the element following the list to null
        }
        return a;
    }

    @SuppressWarnings("unchecked")
    public E get(int i) {
        if (i >= size || i < 0) { // If index is out of bounds
            throw new IndexOutOfBoundsException("Index: " + i + ", Size: " + size); // Throw exception
        }
        return (E) elements[i]; // Return element at index i
    }

    public int size() {
        return size; // Return number of elements in the list
    }

    private void resize() {
        int newSize = elements.length * 2; // Double the size of the array
        Object[] newArray = new Object[newSize]; // Create new array with new size
        System.arraycopy(elements, 0, newArray, 0, size); // Copy elements from old array to new array
        elements = newArray; // Replace old array with new array
    }
}