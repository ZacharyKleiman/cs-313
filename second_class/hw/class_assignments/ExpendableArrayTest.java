import java.util.Arrays;
public class ExpendableArrayTest {
    public static void main(String[] args) {
        ExpendableArray<Integer> array = new ExpendableArray<>();

        // Test add method
        array.add(1);
        array.add(2);
        array.add(3);

        // Test isEmpty method
        System.out.println("Is array empty? " + array.isEmpty());

        // Test size method
        System.out.println("Size of array: " + array.size());

        // Test insert method
        array.insert(1, 4);
        System.out.println("Element at index 1 after insertion: " + array.get(1));

        // Test remove method
        array.remove(1);
        System.out.println("Element at index 1 after removal: " + array.get(1));

        // Test toArray method
        Integer[] arr = array.toArray(new Integer[array.size()]);
        System.out.println("Array after conversion to array: " + Arrays.toString(arr));
        }
}