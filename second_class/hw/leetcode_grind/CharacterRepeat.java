/*
 * You are given a string s and an integer k. You can choose any character of the string and change it to any other uppercase English character. You can perform this operation at most k times.

Return the length of the longest substring containing the same letter you can get after performing the above operations.

 

Example 1:

Input: s = "ABAB", k = 2
Output: 4
Explanation: Replace the two 'A's with two 'B's or vice versa.
 */
class Solution {
    public int characterReplacement(String s, int k) {
        // 1. choose the first character
        // 2. Count the number of instances of that character
        // 3. If the number of instances of that character is greater than k, then choose the next character and check it.
        // 4. Repeat until you find a character that can be done k or less times
        //5. If so, change all instances of that character to the next uppercase character. If that character is Z change it to a
        // 6. Count the longest substring containing only that letter after doing the above
        // 7. return that count
        char[] the_string = s.toCharArray();
        char chooser=the_string[0];
        int counter=counting(the_string, chooser);
        //done step 2
        int i=0;
        while (counter > k) {
            i++;

            chooser=the_string[i];
            counter=counting(the_string, chooser);
        }
        //done step 3
        //done step 4
        for (int j=0;j<the_string.length;j++){
            if (the_string[j]==chooser){
                the_string[j]=(char)(chooser+1);
                if (the_string[j]=='Z'){
                    the_string[j]='A';
                }
            }
        }
        //done step 5.
        // longest substring. Set a variable. Will store longest so far
        int longest_encountered=0;
        int current=0;
        //print the string
        /*for (int t=0;  t<the_string.length;t++){
            System.out.print(the_string[t]);
        }*/
        for (int m=0;m<the_string.length;m++){
            
            if (m==0) {
                current++;
            }
            
            else if (the_string[m]==the_string[m-1]){
                current++;
                if (current>longest_encountered){
                    longest_encountered=current;
                }
                
            }
            else {
                current=1;
            }
        }
        //done step 6
        return longest_encountered;

    }
    public int counting (char[] the_string, char chooser){
        int counter=0;
        for (int i=0;i<the_string.length;i++){
            if (the_string[i]==chooser){
                counter++;
            }
        }
        return counter;
    }
    public static void main(String[] args) {
        Solution mySolution = new Solution();
        System.out.println(mySolution.characterReplacement("ABAB", 2));
    }
}