/*
 Input: s = "abc", shifts = [3,5,9]
Output: "rpl"
Explanation: We start with "abc".
After shifting the first 1 letters of s by 3, we have "dbc".
After shifting the first 2 letters of s by 5, we have "igc".
After shifting the first 3 letters of s by 9, we have "rpl", the answer.

 */
public class ShiftingLetters {
    private String str;
    private int[] shifts;

    public ShiftingLetters(String str, int[] shifts) {
        this.str = str;
        this.shifts = shifts;
    }
    public String shift() {
        char[] chars = str.toCharArray();

    for (int i=0; i<shifts.length; i++){
        for (int j=0; j<=i; j++){
            chars[j]+=shifts[i];
        }
    }
    String str = new String(chars); // This will convert the char array back to a string
    return str;
    }
    public static void main(String[] args) {
        ShiftingLetters shiftingLetters = new ShiftingLetters("abc", new int[]{3, 5, 9});
        System.out.println(shiftingLetters.shift()); // Should print "rpl"
    }
}
