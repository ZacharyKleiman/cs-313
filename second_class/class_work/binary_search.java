import java.util.Arrays;
public class binary_search {
    /*public static int binarySearch(int[] arr, int target) {
        int left = 0;
        int right = arr.length - 1;

        while (arr[left] < arr[right]) {
            int mid = left + (right - left) / 2;
            if (arr[mid] == target) return mid;
            else if (arr[mid] > target) right = mid - 1;
            else left = mid + 1;
        }
        if (arr[left] == arr[right]) {
            if (arr[left] == target) return left;
            else return -1;
        }
        return -1;
    }*/
    public static boolean binarySearch (int arr[], int target, int left, int right) {

        int mid = (right+left)/2;
        if (left > right) {
            return false;
        }
        if (arr[mid] == target) {
            return true;
        }   
        else if (arr[mid] > target) {
            //left=mid+1;
            return binarySearch(arr, target,left,mid-1);
        }
        else if (arr[mid] < target) {
            //right=mid-1;
            return binarySearch(arr, target,mid+1,right);
        }

          return false;
    }
    public static void main(String[] args) {
        int[] arr = {-1,1, 2, 3, 4, 65,323};
        int target = -1;
        int n = arr.length-1;
        System.out.println("The position is: " + binarySearch(arr, target,0,n));
        target=10;
        System.out.println("The position is: " + binarySearch(arr, target,0,n));

    }
}
