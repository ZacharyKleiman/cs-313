public class OpenAddressHashMap<K,V> {
    // Initial capacity of the bucket array
    private static final int INITIAL_CAPACITY = 16;
    // Load factor threshold for resizing
    private static final double LOAD_FACTOR = 0.75;
    // Array of Pairs to store key-value pairs
    private Pair<K,V>[] bucket;
    // Number of key-value pairs in the hashmap
    private int size;
    
    public OpenAddressHashMap() {
        // Initialize the bucket array and size
        bucket = (Pair<K,V>[]) new Pair[INITIAL_CAPACITY];
        size = 0;        
    }
    
    public void put(K key, V value) {
        // Resize the hashmap if the load factor threshold is exceeded
        if (size >= LOAD_FACTOR * bucket.length) {
            resize();
        }
        // Calculate the index for the key
        int index = key.hashCode() % bucket.length;
        // If the bucket is not empty, find the next empty bucket (linear probing)
        while (bucket[index] != null && !bucket[index].getKey().equals(key)) {
            index = (index + 1) % bucket.length;
        }
        // Store the Pair in the bucket
        bucket[index] = new Pair<>(key, value);
        size++;
    }
    
    private void resize() {
        // Create a new array with double the capacity
        Pair<K,V>[] oldBucket = bucket;
        bucket = (Pair<K,V>[]) new Pair[oldBucket.length * 2];
        size = 0;
        // Rehash all the existing Pairs into the new array
        for (Pair<K,V> pair : oldBucket) {
            if (pair != null) {
                put(pair.getKey(), pair.getValue());
            }
        }
    }
    
    public V get(K key){
        // Calculate the index for the key
        int index = key.hashCode() % bucket.length;
        // If the bucket is not empty, find the Pair with the matching key (linear probing)
        while (bucket[index] != null) {
            if (bucket[index].getKey().equals(key)) {
                return bucket[index].getValue();
            }
            index = (index + 1) % bucket.length;
        }
        // If the key does not exist, return null
        return null;
    }
    
    public void remove(K key){
        // Calculate the index for the key
        int index = key.hashCode() % bucket.length;
        // If the bucket is not empty, find the Pair with the matching key and remove it (linear probing)
        while (bucket[index] != null) {
            if (bucket[index].getKey().equals(key)) {
                bucket[index] = null;
                size--;
                return;
            }
            index = (index + 1) % bucket.length;
        }
    }
    
    public LinkedList<K> keySet() {
        // Create a LinkedList to store the keys
        LinkedList<K> keys = new LinkedList<>();
        // Add all the keys to the LinkedList
        for (Pair<K,V> pair : bucket) {
            if (pair != null) {
                keys.insertLast(pair.getKey());
            }
        }
        return keys;
    }
    
    public LinkedList<Pair<K,V>> entrySet(){
        // Create a LinkedList to store the entries
        LinkedList<Pair<K,V>> entries = new LinkedList<>();
        // Add all the entries to the LinkedList
        for (Pair<K,V> pair : bucket) {
            if (pair != null) {
                entries.insertLast(pair);
            }
        }
        return entries;
    }    
}