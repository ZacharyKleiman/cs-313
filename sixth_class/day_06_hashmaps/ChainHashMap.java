import java.util.Iterator;

public class ChainHashMap<K,V> {
    // Initial capacity of the bucket array
    private static final int INITIAL_CAPACITY = 16;
    // Load factor threshold for resizing
    private static final double LOAD_FACTOR = 0.75;
    // Array of LinkedLists to store key-value pairs
    private LinkedList<Pair<K,V>>[] bucket;
    // Number of key-value pairs in the hashmap
    private int size;
    
    public ChainHashMap() {
        // Initialize the bucket array and size
        bucket = (LinkedList<Pair<K,V>>[]) new LinkedList[INITIAL_CAPACITY];
        size = 0;        
    }
    
	public void put(K key, V value) {
        if (size >= LOAD_FACTOR * bucket.length) {
            resize();
        }
        int index = key.hashCode() % bucket.length;
        LinkedList<Pair<K,V>> list = bucket[index];
        if (list == null) {
            list = new LinkedList<>();
            bucket[index] = list;
        }
        LinkedList.Node<Pair<K,V>> curr = list.getFirst();
        while (curr != null) {
            if (curr.getNodeData().getKey().equals(key)) {
                curr.getNodeData().setValue(value);
                return;
            }
            curr = curr.getNodeNext();
        }
        list.insertLast(new Pair<>(key, value));
        size++;
    }    
    private void resize() {
        // Create a new array with double the capacity
        LinkedList<Pair<K,V>>[] oldBucket = bucket;
        bucket = (LinkedList<Pair<K,V>>[]) new LinkedList[oldBucket.length * 2];
        size = 0;
        // Rehash all the existing Pairs into the new array
        for (LinkedList<Pair<K,V>> list : oldBucket) {
            if (list != null) {
                for (Pair<K,V> pair : list) {
                    put(pair.getKey(), pair.getValue());
                }
            }
        }
    }
    
    public V get(K key){
        // Calculate the index for the key
        int index = key.hashCode() % bucket.length;
        LinkedList<Pair<K,V>> list = bucket[index];
        // If the bucket is empty, return null
        if (list == null) return null;
        // If the key exists, return its value
        for (Pair<K,V> pair : list) {
            if (pair.getKey().equals(key)) {
                return pair.getValue();
            }
        }
        // If the key does not exist, return null
        return null;
    }
    

    public void remove(K key){
        int index = key.hashCode() % bucket.length;
        LinkedList<Pair<K,V>> list = bucket[index];
        if (list == null) return;
        LinkedList.Node<Pair<K,V>> curr = list.getFirst();
        LinkedList.Node<Pair<K,V>> prev = null;
        while (curr != null) {
            if (curr.getNodeData().getKey().equals(key)) {
                if (prev == null) {
                    list.removeFirst();
                } else if (curr.getNodeNext() == null) {
                    list.removeLast();
                } else {
                    prev.setNodeNext(curr.getNodeNext());
                    size--;
                }
                return;
            }
            prev = curr;
            curr = curr.getNodeNext();
        }
    }    
    public LinkedList<K> keySet() {
        // Create a LinkedList to store the keys
        LinkedList<K> keys = new LinkedList<>();
        // Add all the keys to the LinkedList
        for (LinkedList<Pair<K,V>> list : bucket) {
            if (list != null) {
                for (Pair<K,V> pair : list) {
                    keys.insertLast(pair.getKey());
                }
            }
        }
        return keys;
    }
    
    public LinkedList<Pair<K,V>> entrySet(){
        // Create a LinkedList to store the entries
        LinkedList<Pair<K,V>> entries = new LinkedList<>();
        // Add all the entries to the LinkedList
        for (LinkedList<Pair<K,V>> list : bucket) {
            if (list != null) {
                for (Pair<K,V> pair : list) {
                    entries.insertLast(pair);
                }
            }
        }
        return entries;
    }    
}