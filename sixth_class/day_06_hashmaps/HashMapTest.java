import org.junit.Test;
import static org.junit.Assert.*;

public class HashMapTest {

    @Test
    public void testChainHashMap() {
        ChainHashMap<String, Integer> map = new ChainHashMap<>();
        map.put("John", 32);
        assertEquals(Integer.valueOf(32), map.get("John"));
        map.remove("John");
        assertNull(map.get("John"));
        assertTrue(map.keySet().isEmpty());
        assertTrue(map.entrySet().isEmpty());
    }

    @Test
    public void testOpenAddressHashMap() {
        OpenAddressHashMap<String, Integer> map = new OpenAddressHashMap<>();
        map.put("John", 32);
        assertEquals(Integer.valueOf(32), map.get("John"));
        map.remove("John");
        assertNull(map.get("John"));
        assertTrue(map.keySet().isEmpty());
        assertTrue(map.entrySet().isEmpty());
    }
}